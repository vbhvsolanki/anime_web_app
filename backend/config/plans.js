module.exports = {
  free_plan_ids: [{
    id: 0,
    price: 0,
    duration: -1
  },{
    id: 1,
    price: 0,
    duration: -1
  }],
  paid_plan_ids: [{
    id: 2,
    price: 200,
    duration: 30
  },{
    id: 3,
    price: 2200,
    duration: 365
  }],
}